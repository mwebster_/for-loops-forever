# FOR_Loops_Forever Workshop
WORKSHOP on FOR loops, their history, their structure and their possibilities for creating graphic form.

[![PyPI](https://img.shields.io/pypi/l/fsfe-reuse.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/reuse/reuse)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
---

![bouclesFOREver](https://cloud.githubusercontent.com/assets/1027891/5423856/33dd8ebe-82d4-11e4-8caf-e90866e30b8d.jpg)


## Presentation

The workshop was given 8 novemember 2014 at Paris College of Art, Paris. Original title , *Dessines moi des boucles FOR ever*. The aim of the workshop was to present a little of the history behind one of the most important programming structures and introduce its workings. I chose to concentrate on one particular application of FOR loops from a graphic design perspective. Put plainly, we worked on how repetition can be used to design visual patterns. 

All sketches we coded in Processing and are the examples used to introduce key concepts for the participants to learn and develop their own ideas. 


## Contents

* Basics
* Moving On
* Sundries - some simple application of ideas
* Participants - sketches by some participants
* Libs - third party libraries used


## Install

The programs found in this repository can be compiled using the Processing environment.

[https://processing.org/](https://processing.org/)


## Contact & Sundries

* mark.webster[at]wanadoo.fr
* Version v1.0
* Tools used : Processing

## Contribute
To contribute to this project, please contact me at the above email address.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/


